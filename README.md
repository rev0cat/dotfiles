# dotfiles

rev0cat's dotfiles and personal handbook


## Files

    config/
    ├── README
    ├── emacs.el            # TODO
    ├── zshrc               # TODO
    ├── Xresources          # TODO
    └── gentoo/
        ├── make.conf       # TODO
        ├── use             # TODO
        └── keywords        # TODO


    handbook/
    ├── README              # TODO
    ├── gnupg               # TODO: revoking, apps integration
    ├── git                 # TODO
    ├── firefox             # TODO
    ├── shell               # TODO
    ├── gentoo/
    │   ├── README          # TODO
    │   ├── install         # TODO
    │   ├── network         # TODO
    │   ├── packages        # TODO
    │   ├── security        # TODO
    │   ├── kernel          # TODO
    │   └── desktop         # TODO
    └── emacs/
        ├── README          # TODO
        ├── config          # TODO
        ├── aesthetics      # TODO
        ├── completion      # TODO
        ├── edit            # TODO
        ├── markdown        # TODO
        ├── org-mode        # TODO
        ├── magit           # TODO
        ├── projectile      # TODO
        ├── dired           # TODO
        ├── tramp           # TODO
        ├── languages       # TODO
        └── elisp           # TODO
