# GNU Privacy Guard

*GNU Privacy Guard* (GPG or GnuPG) is a libre implementation of
OpenPGP standard. It provides cryptographic tools for encrypting and
signing data and communication, indentity and integrity validation,
managing keys and connecting to identity key servers. Default
key-server of GnuPG is `hkp://keys.gnupg.net`.


## Sources

- [thoughtbot](https://www.youtube.com/watch?v=Lq-yKJFHJpk) screencast
  "A Pretty Good Introduction to Pretty Good Privacy"
- [madboa.com](https://www.madboa.com/geek/gpg-quickstart/) GPG Quick Start
- [futureboy.us](https://www.futureboy.us/pgp.html) Alan Eliasen GPG Tutorial


## Vocabulary

**PGP** (Pretty Good Privacy) is the original proprietary licensed
application. **OpenPGP** is open-source version of PGP that lately
settled a standard. **GnuPG** (GNU Privacy Guard) is GNU's libre PGP
implementation based on OpenPGP standard. It offers **keyring** for
storing and manipulating with keys on local database and **gpg-agent**
to mediate access to keyring for many applications from Linux/UNIX
ecosystem such as email clients, Git, password managers, editors, etc.

**Fingerprint** of public key is its hash that one can rely on when
getting public key from key server. *Long key ID* and *short key ID*
are parts of fingerprint only meant for manipulation within the
applications and should not be used for verification of public
key. (Short keys are not used anymore for security reasons.)

    Fingerprint: EC23 92F2 EDE7 4488 680D A3CF 5F2B 4756 ED87 3D23
    Long key ID:                               5F2B 4756 ED87 3D23
    Short key ID: (no longer used)                       ED87 3D23

**Public** key and **private** (secret) key together can do two
operations: **encryption**: public key can encrypt message that only
private key can decrypt, and **signing**: private key can create
signature of message that public key can validate.

**Web of Trust** is concept of public key infrastructure where key
owners sign keys of other trusted owners (with their private key) to
create network of signed keys for authentication. It is decentralized
version of certificate authority of SSL certificates. **Key servers**
are where publishing and signing of keys take place.


## Key management

### Generate new key-pair

Default and recommended type is *rsa2048*. Generate new key-pair:

~~~ sh
gpg --gen-key
# enter name, email and passphrase
~~~

All name, email, fingerprint and key ID (long) can be used for
manipulation within keyring and for search on key servers (in examples
refered to as `'name'`). Passphrase locks the private key on local
database.


### List keys in keyring

List public keys:

~~~ sh
gpg --list-keys
gpg --fingerprint
~~~

List private keys: (does not print them)

~~~ sh
gpg --list-secret-keys
~~~


### Import public keys

Import full public key (usually `.asc` file extension):

~~~ sh
gpg --import key.asc
~~~

Search on public keyserver:

~~~ sh
gpg --keyserver hkp://keys.gnupg.net --search-key 'name'
~~~


### Delete keys

When imported key is no longer needed it can be deleted from keyring.

~~~ sh
gpg --delete-keys 'name'
~~~

Private key can also be deleted, but it should be revoked - protocol
verifying that private key is no longer used (might be compromised) is
distributed to key servers and others.


### Share public key

Public keys can be listed on key servers. There are many of them,
default is GNU's `hkp://keys.gnupg.net`.

~~~ sh
gpg --send-keys 'name'
gpg --send-keys 'name' --keyserver hkp://keys.gnupg.net
~~~

Export full public key in ASCII (`--armor` or `-a` flag). Might be
alternative to keyserver if public keys should not leave group.

~~~ sh
gpg --armor --output mypubkey.asc --export 'name'
gpg --armor --export 'name' > mypubkey.asc
~~~


### Backup keys

Export private key: (asks for passphrase)

~~~ sh
gpg --armor --export-secret-key 'name' > private.key
~~~


## Encryption and decryption

To encrypt file, specify recipient and optionally output file with
`--output`, otherwise it creates same filename with `.gpg` extension.

~~~ sh
gpg --encrypt --recipient 'name' file.txt
gpg -e -r 'name' file.txt
~~~

However this `.gpg` file is in binary format. To produce message in
ASCII that can be send by mail, use `-a` or `--armor` flag. Message
can be also redirected from stdin.

~~~ sh
echo "This is private message." | gpg --armor --encrypt --recipient 'name'
echo "This is private message." | gpg -aer 'name'

-----BEGIN PGP MESSAGE-----

hQEMA0DF2vGOQjY5AQf8Dy+gUkDjETBTSXx7m9ORnBoqXzcK55ZzxF4SlJhBQMeO
oLiEpI6GRsPO2NEplSVttRNHsbxhxNhyF5oSpUO5XPsXcY0xX5h9loXg9JcOvGdL
Hj03+4C9rvddH8IyoTG8DRM7NTr8Vu7bAzHsdCdGu6xRUV3tFAydx3S3YWFkORoq
SVSFnzhmjWfAnvgAd79vBVw5mBhNwCYPrxK1NvmnnmYSfDDCcDxmnuO+UEibb/g7
7mI0qPV5cIXB39kAbJkdxO4kzfsIthJ4Wwx1S0iBikGjWC6CBOamuj6Sh6Q/eFfr
YyWNEgLDMiXA0tySMrWsq9YexccGeqvcyhow2tH2XNJTAepHTz4Fir8/JJz0Kaj2
Mn/x167m6T0mOU2Ayaw+m9Spnz+0OitiypczXzPiLXX7bJQX6aU+YRss6rF2bCdY
1LSmC5D9Op9d/e38tVUsVNQMTnA=
=PJsY
-----END PGP MESSAGE-----
~~~

Decrypting file requires private key and its passphrase. Somehow it
knows recipient automatically? (TODO more private keys) Asks for
passphrase.

~~~ sh
gpg --output file.txt --decrypt file.txt.gpg
~~~


## Signatures

Signature is generated with private key and verified with public
key. That can approve both file consistency and origin.

It usually comes as detached signature file (`.asc` extension). Verify
file and detaches signature:

~~~ sh
gpg --verify archive.tgz.asc archive.tgz
~~~

Create detached signature for file:

~~~ sh
gpg --armor --detach-sign archive.tgz
~~~

To sign cleartext message, use `--clearsign` flag. This automacically
produces ASCII signature.

~~~ sh
gpg --clearsign filename
echo "This is public message." | gpg --clearsign

-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

This is public message.
-----BEGIN PGP SIGNATURE-----

iQEzBAEBCAAdFiEEWpVlr4ei39ShSZ9+2ZeKAhNOqrgFAljX6JkACgkQ2ZeKAhNO
qri7pgf/Xx2LPUaXe5cYbLosFjp/kWqrG0+4db/1GjY0fY/HWkJnPr/pSICw4Rn3
jxuKZM02aLnjYCj3QYiSuZQg2PSOGYm7Z+pw2mPar6zW2yinicp4MP588MokEiis
0OdafyYliuE4eQviaf5yDYc6BeYzf6bMj+EeVJajJ8LyIupa9mxNWF8eSz2rirdT
6ig2dzVxoY7CPLqh5s0gLpHymqrz/CAP65qJR8HwthtG9UOVLtqWr0shH1QcedaG
j0MZryfOlVhnLyTZcNpwgOXuapXuAFsUhdyvecRThACZ6w34XKsRBhJcjSln+kuJ
r+xfJIeWTrdAELn8xjXMg7BL72YOjw==
=ymnW
-----END PGP SIGNATURE-----
~~~


## Examples

### Verify Gentoo ISO

Public key of Gentoo releases is located at `Downloads->Signatures`
page. The relevant key for ISOs and releases is `0xBB572E0E2D182910`.
Download the full public key from keyserver.

~~~sh
gpg --keyserver hkp://keys.gnupg.net --recv-keys 0xBB572E0E2D182910
~~~

Download ISO and related files. `.iso` is the installation media,
`.CONTENTS` is listing of all files available on installation meda,
`.DIGESTS` file contains the hashes of ISO and `.DIGESTS.asc` contains
both hashes and cryptographic signature of ISO. For verification only
`.iso` and `.DIGESTS.asc` are needed.

Verify cryptographic signature of `.DIGESTS.asc` file.

~~~sh
gpg --verify install-amd64-minimal-20141204.iso.DIGESTS.asc

gpg: Signature made Fri 21 Apr 2017 10:21:13 PM CEST
gpg:                using RSA key 13EBBDBEDE7A12775DFDB1BABB572E0E2D182910
gpg: Good signature from "Gentoo Linux Release Engineering (Automated Weekly Release Key) <releng@gentoo.org>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 13EB BDBE DE7A 1277 5DFD  B1BA BB57 2E0E 2D18 2910
gpg: WARNING: not a detached signature; file 'install-amd64-minimal-20170420.iso.DIGESTS' was NOT verified!
~~~

Output says that `.DIGESTS` file was signed with private key of Gentoo
Linux Release Engineering team, however we don't have trusted
signature chain to be certain. Now validate the hashes.

~~~sh
sha512sum -c install-amd64-minimal-20170420.iso.DIGESTS.asc

install-amd64-minimal-20170420.iso: OK
install-amd64-minimal-20170420.iso: FAILED
install-amd64-minimal-20170420.iso.CONTENTS: OK
install-amd64-minimal-20170420.iso.CONTENTS: FAILED
sha512sum: WARNING: 19 lines are improperly formatted
sha512sum: WARNING: 2 computed checksums did NOT match
~~~

Note that FAILED lines are actually Whirlpool hashes and so they don't
check. The OK lines are indeed sha512 checksums.
